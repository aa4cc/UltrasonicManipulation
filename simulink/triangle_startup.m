%% Add folders to path
addpath('system_object/bfgs','system_object/newGenerator','controller_constants/triangle','MATLAB_scripts');
%% Load constants
load('correction.mat');
load('H_unrot.mat');