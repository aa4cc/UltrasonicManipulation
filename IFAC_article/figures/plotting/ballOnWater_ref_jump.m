load('ref_jump.mat');

figure
clf
subplot(2,1,1)
plot(t,ballX*1000,'LineWidth',1);
hold on
plot(t,refX*1000,'LineWidth',1);
xlabel 'Time [s]'
ylabel 'Position [mm]'
legend('Object position','Reference');
title 'Response to jump of reference'
subplot(2,1,2)
plot(t,P,'LineWidth',1)
ylim([500 2750])
xlabel 'Time [s]'
ylabel 'Pressure [Pa]'
title 'Control action'