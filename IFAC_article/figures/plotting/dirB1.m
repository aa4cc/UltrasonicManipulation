function [ value ] = dirB1( angle )
f = 40000; % 40 kHz frekvence vysilace
w = 2*pi*f;
c = 346.3 ;% rychlost zvuku v m/s
k = w/c;
r = 0.00485; % 0.5 cm polomer
value = angle;
for q = 1: numel(angle)
    if angle(q) == 0
        value(q) =1;
    else
value(q) = 2*besselj(1,k*r*sin(angle(q)))/(k*r*sin(angle(q)));
    end

end

