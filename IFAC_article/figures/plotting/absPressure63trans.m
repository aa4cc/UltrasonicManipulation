function p = absPressure63trans(xCoord,yCoord,zCoord)
    f = 40000; % 40 kHz frekvence vysilace
    w = 2*pi*f;
    c = 340; % rychlost zvuku v m/s
    k = w/c;
    r = 0.005; % 0.5 cm polomer
    P = 6.8; % konstanta vysilace
    phi = zeros(63,1);
    %phi = phases;
    %numberOfTrans = 8;

    transx = linspace(-r*7,r*7,8);
    transy = linspace(-r*7,r*7,8);
    [transx, transy] = meshgrid(transx, transy);
    transx = reshape(transx,[64,1]);
    transy = reshape(transy,[64,1]);
    transx = transx(1:63);
    transy = transy(1:63);
    
    [x,y,z] = meshgrid(xCoord,yCoord,zCoord);
    %[x,z] = meshgrid(x,z);

    dirFunc = @dirB1;

    p = 0;

    for idx = 1:63
            xr = x-transx(idx);
            yr = y-transy(idx);
            d = sqrt(xr.^2+yr.^2+z.^2);
            theta = asin(sqrt(xr.^2+yr.^2)./d);
            M = P*dirFunc(theta).*exp(1j*k*d)./d;
            p = p + M*exp(1j*phi(idx));
    end
    p = abs(p);
end