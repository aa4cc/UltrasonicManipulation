%% Constants
% 85 mV ~ 94 dB
% ref. pressure 2*10^-5 Pa
p_ref = 2*10^(-5);
u_0 = 0.085;
dB = 134;

% dB = 20*log(p_0/p_ref)
p_0 = 10^(dB/20)*p_ref;

% p_0 = u_0*P_konst
P_konst = p_0/u_0;

%% Measurement - power
load('measure_power_10mm.mat');
P_measure = reshape(voltage,101,1)*P_konst;

% model - curve fitting
P0 = 6.8;
bias = -16.48;
d = linspace(0.03,0.08,200);
P_model = P0./d + bias;

%% Plotting - power
figure
plot(zCoord*1000,P_measure,'.')
ax = gca;
ax.FontSize = 12;
hold on
plot(d*1000,P_model,'--','LineWidth',1.5)
xlabel('Distance [mm]','FontSize',12)
ylabel('Pressure [Pa]','FontSize',12)
title('Transducer power','FontSize',12)
legend('Measurement','Identified model','FontSize',11)
xlabel('Distance [mm]','FontSize',12)
ylabel('Pressure [Pa]','FontSize',12)
grid on

%% Measurement - field
load('measure_field.mat');
F_measure = reshape(U,81,41)'*P_konst;
zOffset = 0.003;
F_model = reshape(absPressure63trans(xCoord,yCoord,zCoord+zOffset),81,41)';

%% Plotting - field
figure
subplot(1,10,1:4)
imagesc(yCoord*1000,zCoord*1000,F_measure)
ax = gca;
ax.FontSize = 12;
title('Measurement','FontSize',12)
xlabel('y [mm]','FontSize',12)
ylabel('z [mm]','FontSize',12)
colormap hot
caxis([200 1200])
% c = colorbar;
% c.Label.String = 'Pressure [Pa]';
set(gca,'YDir','normal')

subplot(1,10,6:10)
imagesc(yCoord*1000,zCoord*1000,F_model)
ax = gca;
ax.FontSize = 12;
title('Model','FontSize',12)
xlabel('y [mm]','FontSize',12)
ylabel('z [mm]','FontSize',12)
colormap hot
caxis([200 1200])
c = colorbar;
c.Label.String = 'Pressure [Pa]';
set(gca,'YDir','normal')