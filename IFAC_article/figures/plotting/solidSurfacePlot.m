%% clear workspace and load experiment data
clear all
load('kruhPID.mat')

%% plot whole experiment to choose good time interval
figure
t = Mereni.time;
x = Mereni.signals(1).values ;
y = Mereni.signals(2).values ;
xr = Mereni.signals(3).values ;
yr = Mereni.signals(4).values ;
plot(t,x,t,y,t,xr,t,yr)
legend('x','y','xref','yref')
xlim([0 130])

%% choos time interval
start_time = 32;
end_time = 34;
tkoef = ((start_time/0.02)+1):((end_time/0.02)+1);
figure
title ('Tracking a moving reference � BS','FontSize',12);
subplot(10,10,[5:10,15:20,25:30.35:40,45:50,55:60,65:70,75:80,85:90])
tspan = t(tkoef);
plot(tspan-34,x(tkoef)*1000,tspan-34,xr(tkoef)*1000,'-.',tspan-34,y(tkoef)*1000,tspan-34,yr(tkoef)*1000,'--g','LineWidth',1.5)
legend({ 'x','x-reference','y','y-reference'},'Location','best','FontSize',12,'NumColumns',2)
% legend({ 'x-coordinate','x-reference','y-coordinate','y-reference'},'Location','southeast','FontSize',12)
xlabel('Time [s]','FontSize',12)
ylabel('Position [m]','FontSize',12)
ylim([-0.007*1000 0.007*1000]);
grid on
set(gca, 'FontSize', 12)
set(gca,'YDir','normal')
subplot(10,10,[1:3,11:13,21:23.31:33,41:43,51:53,61:63,71:73,81:83])
plot(x(tkoef)*1000,y(tkoef)*1000,xr(tkoef)*1000,yr(tkoef)*1000,'--','LineWidth',1.5)
ax = gca;
ax.FontSize = 12; 
legend({ 'Position','Reference'},'Location','best','Orientation','Horizontal','FontSize',11)
xlabel('x [mm]','FontSize',12)
ylabel('y [mm]','FontSize',12)
grid on
ylim([-0.01*1000 0.01*1000]);
xlim([-0.01*1000 0.01*1000])
 pbaspect([1 1 1])
% pbaspect([1 1 1])
set(gca, 'FontSize', 12)
set(gca,'YDir','normal')
