load('eight.mat')

figure
subplot(10,10,[6:10,16:20,26:30.36:40,46:50,56:60,66:70,76:80,86:90])
plot(t,ballX*1000,t,refX*1000,'-.',t,ballY*1000,t,refY*1000,'--g','LineWidth',1.5)
legend({ 'x','x-reference','y','y-reference'},'Location','best','FontSize',12,'NumColumns',2)
xlabel('Time [s]','FontSize',12)
ylabel('Position [m]','FontSize',12)
grid on
set(gca, 'FontSize', 12)
set(gca,'YDir','normal')
subplot(10,10,[1:4,11:14,21:24.31:34,41:44,51:54,61:64,71:74,81:84])
plot(ballX*1000,ballY*1000,'LineWidth',1.5)
ax = gca;
ax.FontSize = 12; 
hold on
plot(refX*1000,refY*1000,'--','LineWidth',1.5)
legend({ 'Position','Reference'},'FontSize',11,'Orientation','Horizontal','Location','Best');
xlabel('x [mm]','FontSize',12)
ylabel('y [mm]','FontSize',12)
grid on