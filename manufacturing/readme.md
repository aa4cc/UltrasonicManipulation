# Manufacturing
[index](../README.md) \> [manufacturing](readme.md)

For the instruction on building the platform, please refer to [mechanical assembly](../docs/assembly.md)

## 3D print

The models fro 3D printing are made in openSCAD and should be easily converted to STL for printing.
We printed our models using Ultimaker 2+ and Ultimaker S5, mostly from PLA.

## Laser-cut

The drawings were originally made in Autodesk Fusion and exported to DXF.
The drawings are split into layers, named _Cut_ or _Engrave_.
We made the laser-cut parts using Epilog Mini 18.

## PCB

The PCBs are created in Eagle.
For manufacturing, we've had good experience with [allPCB](https://www.allpcb.com/).